--vim.cmd "let g:vimtex_view_general_viewer = 'okular'"
--vim.cmd "let g:vimtex_view_method = 'zathura'"
--vim.cmd "let g:vimtex_view_general_options = '--unique file:@pdf\\#src:@line@tex'"
--vim.cmd "let g:vimtex_view_general_options = '-reuse-instance -forward-search @tex @line @pdf'"
-- vim.cmd "let g:vimtex_view_general_options_latexmk = '-reuse-instance'"
--vim.cmd "nmap <F12> :VimtexCompile<cr>"

vim.cmd([[
  let g:vimtex_view_method = 'zathura'
  let g:vimtex_view_general_options = '--unique file:@pdf\\#src:@line@tex'
  nmap <F12> :VimtexCompile<cr>
]])
